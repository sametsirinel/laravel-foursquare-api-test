<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@backend')->name("home");
Route::get('/backend', 'HomeController@backend')->name("backend");
Route::get('/backend/{categoryId}', 'HomeController@near')->name("category");
Route::get('/backend/request/{url}', 'HomeController@request')->name("request");
