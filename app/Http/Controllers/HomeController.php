<?php

namespace App\Http\Controllers;

use Ixudra\Curl\Facades\Curl;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller{

    public function backend(){

        $response = $this->request("categories");

        if ($response->meta->code != 200) {
            return response(view("500", ["error" => "Connection Problem"]),500);
        }
        
        $categories = $response->response->categories ?? [];

        return View("backend",[
            "categories"=> $categories
        ]);

    }

    public function near(Request $request,$categoryId = null){

        $response = $this->request("explore",[
            "near" => $request->input("city") ?? 'valletta',
            "categoryId" => $categoryId,
        ]);

        if ($response->meta->code != 200) {
            return response(view("500", ["error" => "Connection Problem"]),500);
        }

        $places = $response->response->groups[0]->items ?? [];

        return View("places", [
            "places" => $places
        ]);

    }

    protected function request($url = "",$data = []){
        
        try {

            $data = $this->readyData($data);

            return Curl::to('https://api.foursquare.com/v2/venues/' . $url)
                ->withData($data)
                ->asJson()
                ->get();

        } catch (Exception $ex) {

            return response(view("500",["error" => $ex]),500);

        }

    }

    public function readyData($data = []){

        $array = [
            "client_id"=>"4KSPFVYT4BSOQ14UXYDUDMD3L5HNCVQIYURGLC3KBEPCLR4D",
            "client_secret"=> "P0YFBCGUTCAC4VOLUSD15RTV0FX43JSR2VX4XNM0DDWVAOTH",
            "v" => "20180323",
        ];

        if(!is_array($data)){
            return $array;
        }

        return array_merge($array,$data); 

    }

}
