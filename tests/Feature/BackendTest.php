<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Ixudra\Curl\Facades\Curl;

class BackendTest extends TestCase{
    
    /** @test */
    public function is_200_ok(){

        $response = $this->get('/');

        $response->assertStatus(200);

        $response = $this->get('/backend');

        $response->assertStatus(200);
        
    }

    /** @test */
    public function checkCategoryPlace(){

        $response = Curl::to('https://api.foursquare.com/v2/venues/categories')
            ->withData([
                "client_id" => "4KSPFVYT4BSOQ14UXYDUDMD3L5HNCVQIYURGLC3KBEPCLR4D",
                "client_secret" => "P0YFBCGUTCAC4VOLUSD15RTV0FX43JSR2VX4XNM0DDWVAOTH",
                "v" => "20180323",
            ])
            ->asJson()
            ->get();

        if ($response->meta->code != 200) {
            return view("500", ["error" => "Connection Problem"]);
        }

        $category = $response->response->categories[0]->id ?? '';

        $response = $this->get('/backend/'. $category);

        $response->assertStatus(200);

    }

    /** @test */
    public function checkCategoryWithNear(){

        $response = Curl::to('https://api.foursquare.com/v2/venues/categories')
            ->withData([
                "client_id" => "4KSPFVYT4BSOQ14UXYDUDMD3L5HNCVQIYURGLC3KBEPCLR4D",
                "client_secret" => "P0YFBCGUTCAC4VOLUSD15RTV0FX43JSR2VX4XNM0DDWVAOTH",
                "v" => "20180323",
                "near"=> "bursa"
            ])
            ->asJson()
            ->get();

        if ($response->meta->code != 200) {
            return view("500", ["error" => "Connection Problem"]);
        }

        $category = $response->response->categories[0]->id ?? '';

        $response = $this->get('/backend/'. $category,[
            "city"=>"istanbul"
        ]);

        $response->assertStatus(200);

    }

}
