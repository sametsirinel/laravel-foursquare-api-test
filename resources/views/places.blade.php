@extends('layouts.header')
    <div class="container">
        <div class="py-5 text-center">
            <a  href="{{ route('backend')}}" class="btn btn-primary btn-lg">Change Category</a>
            <h2 class="mt-5">Places Page</h2>
            @if($places[0]->venue->categories[0]->name ?? false)
                <h3>You Loking '{{ $places[0]->venue->categories[0]->name ?? '' }}' Category</h3>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 mb-4">
                <form>
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" name="city" value="{{request()->input('city') ?? ''}}" class="form-control" placeholder="City Name">
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-block btn-outline-dark "> Search near on the city </button>
                        </div>
                    </div>
                </form>
            </div>
            @foreach($places as $key=>$place)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="map" id="map{{$key}}"></div>
                          
                            <h4>{{ $place->venue->name ?? '' }}</h4>
                            <p>{{ $place->venue->location->formattedAddress[0] ?? '' }}</p>
                            <p>{{ $place->venue->location->formattedAddress[1] ?? '' }}</p>
                            <p>{{ $place->venue->location->formattedAddress[2] ?? '' }}</p>
                        </div>
                    </div>
                </div>
            @endforeach

            <script>
                function initMap() {
                    @foreach($places as $key=>$place)
                        var map{{$key}};
                        map{{$key}} = new google.maps.Map(document.getElementById('map{{$key}}'), {
                            center: {lat: {{ $place->venue->location->lat ?? '' }}, lng: {{ $place->venue->location->lng ?? '' }}},
                            zoom: 18
                        });

                        var marker{{$key}} = new google.maps.Marker({position: {lat: {{ $place->venue->location->lat ?? '' }}, lng: {{ $place->venue->location->lng ?? '' }}}, map: map{{$key}}});
                    @endforeach
                }
            </script>
        </div>
    </div>

 <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZlfntN04dar1cOLOY3Gef_igXp5vyCGU&callback=initMap">
</script>
<style>
  .map {
    height: 200px;
  }

  .card {
      min-height:450px;
      margin-bottom:10px;
  };
</style>
@extends('layouts.footer')