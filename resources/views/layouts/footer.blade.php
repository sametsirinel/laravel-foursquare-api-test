
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script>

        $(function(){

            $(".openlvl1").click(function(){

                $(".lvl1").addClass("d-none");
                $(".lvl2").addClass("d-none");
                var id = $(this).attr("data-id");
                $("#"+id).toggleClass("d-none");

            });

            $(".openlvl2").click(function(){

                $(".lvl2").addClass("d-none");
                var id = $(this).attr("data-id");
                
                var count = $("#"+id).length;

                console.log(count);

                if(count>0)
                  $("#"+id).toggleClass("d-none");
                else
                  $("#lvl3nofound").toggleClass("d-none")

            });

        });

    </script>
  </body>
</html>