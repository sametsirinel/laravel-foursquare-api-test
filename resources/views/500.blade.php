@extends('layouts.header')
    <div class="container">
        <div class="py-5 text-center">
            <a  href="{{ route('backend')}}" class="btn btn-primary btn-lg">Select Category</a>
            <h1 class="mt-5 ">500 Server Error</h1>
            <h3 class="lead"> {{ $error }}</h3>
        </div>
    </div>
@extends('layouts.footer')