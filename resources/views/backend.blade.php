@extends('layouts.header')
    <div class="container">
        <div class="py-5 text-center">
            <h2 class="mt-5">Backend Page</h2>
        </div>

        <div class="row">
            <div class="col-md-4">
                <ul class="list-group mb-3">
                    @foreach($categories ?? [] as $category)
                        <li class="list-group-item d-flex justify-content-left align-item-center lh-condensed">
                            <span class="text-muted bg-dark">
                                <img src="{{ ($category->icon->prefix ?? '')."64".($category->icon->suffix ?? '') }}" alt="">
                            </span>
                            <a href="Javascript:;" data-id="{{ $category->id ?? '' }}" class="text-dark ml-4 openlvl1">
                                <h6 class="my-0">{{ $category->name ?? '' }}</h6>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @foreach($categories ?? [] as $category)
                <div class="col-md-4 d-none lvl1" id="{{ $category->id ?? '' }}">
                    <ul class="list-group mb-3">
                        @foreach ($category->categories ?? [] as $item)
                            <li class="list-group-item d-flex justify-content-left align-item-center lh-condensed">
                                <span class="text-muted bg-dark"><img src="{{ ($item->icon->prefix ?? '')."64".($item->icon->suffix ?? '') }}" alt=""></span>
                                <a href="{{ count($item->categories ?? []) > 0 ? 'Javascript:;' : route("category",[ "categoryId" => urlencode($item->id ?? '') ]) }}" data-id="{{$item->id ?? ''}}" class="text-dark ml-4 openlvl2">
                                    <h6 class="my-0">{{ $item->name ?? '' }}</h6>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
            @foreach($categories ?? [] as $category)
                @foreach ($category->categories ?? [] as $item)
                    <div class="col-md-4 d-none lvl2" id="{{$item->id}}">
                        <ul class="list-group mb-3">
                            @foreach ($item->categories ?? [] as $item2)
                                <li class="list-group-item d-flex justify-content-left align-item-center lh-condensed">
                                    <span class="text-muted bg-dark">
                                        <img src="{{ ($item2->icon->prefix ?? '')."64".($item2->icon->suffix ?? '') }}" alt="">
                                    </span>
                                    <a href="{{ route("category",[
                                        "categoryId"=>urlencode($item2->id ?? '')
                                    ]) }}" class="text-dark ml-4">
                                        <h6 class="my-0">{{ $item2->name ?? '' }}</h6>
                                    </a>
                                </li>
                             @endforeach
                        </ul>
                    </div>
                @endforeach
            @endforeach
             <div class="col-md-4 d-none lvl2" >
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-left align-item-center lh-condensed">
                        <h2>Category Not Found</h2>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@extends('layouts.footer')