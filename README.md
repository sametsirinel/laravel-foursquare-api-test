<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Install Project

git clone git@gitlab.com:sametsirinel/foursquare-api-test.git

cd foursquare-api-test

composer update

php artisan serve
